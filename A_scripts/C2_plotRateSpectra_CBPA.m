%% load subject list

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.data = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';

for indSubject = 1:numel(IDs)
    disp(['Subject ', num2str(indSubject)])
    load([pn.data, 'RhythmRate_',IDs{indSubject},'.mat'])
    Events_Merged(indSubject,:,:,:) = Events;
    EventsNonRhythm_Merged(indSubject,:,:,:) = EventsNonRhythm;
    EventsRhythm_Merged(indSubject,:,:,:) = EventsRhythm;
end

save([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

%% plot

freq = 2.^[1:.125:6];
freq = freq(4:end-3);

figure;
subplot(3,2,1); imagesc(squeeze(nanmean(Events_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,2); imagesc(squeeze(nanmean(Events_Merged(:,:,:,2),1))./...
    squeeze(nanmean(Events_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,3); imagesc(squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,4); imagesc(squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,2),1))./...
    squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,5); imagesc(squeeze(nanmean(EventsRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,6); imagesc(squeeze(nanmean(EventsRhythm_Merged(:,:,:,2),1))./...
    squeeze(nanmean(EventsRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
 
%% load stats cluster

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

%% correlate with individual MSE values

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat')

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

addpath('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/T_tools/')

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/C_figures/';
figureName = 'C2_CorrelationMSE_EventRate';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,3,1); cla; hold on;
    x = BetaMSE_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = BetaMSE_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Beta Entropy'); ylabel('Alpha Entropy');
    title('Parietooccipital Alpha')
subplot(1,3,2); cla; hold on;
    x = BetaRate_YA; y = AlphaRate_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaRate_OA; y = AlphaRate_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Number of Alpha events');
    title('Central Beta')
subplot(1,3,3); cla; hold on;
    x = AlphaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = AlphaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Beta Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% CBPA of individual MSE correlations

addpath('/Volumes/EEG/BOSC_Sternberg/T_tools/fieldtrip-20180227'); ft_defaults;

load('/Volumes/EEG/BOSC_Sternberg/A_data/elec.mat')

ageIdx{1} = 1:47;
ageIdx{2} = 48:99;

freq = 2.^[1:.125:6];
freq = freq(4:end-3);

indCond = 2;
for indAge = 1:2
    StatStruct{indAge}.ratespectrum = squeeze(EventsRhythm_Merged(ageIdx{indAge},:,:,indCond));
    StatStruct{indAge}.dimord = 'rpt_chan_freq';
    StatStruct{indAge}.label = elec.label(1:60);
    StatStruct{indAge}.freq = freq;
end

%% calculate CBPA

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label(1:60);

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 3;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1});

BetaMSE{1} = BetaMSE_YA;
BetaMSE{2} = BetaMSE_OA;
AlphaMSE{1} = AlphaMSE_YA;
AlphaMSE{2} = AlphaMSE_OA;

stat = [];
for indGroup = 1:2
    subj = size(StatStruct{indGroup}.ratespectrum,1);
    design = []; design(1,1:subj) = AlphaMSE{indGroup};
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.parameter = 'ratespectrum';
    [stat{indGroup}] = ft_freqstatistics(cfgStat, StatStruct{indGroup});
end

figure; 
subplot(2,2,1); imagesc(stat{1}.stat.*stat{1}.mask)
subplot(2,2,2); imagesc(stat{2}.stat.*stat{2}.mask)
subplot(2,2,3); imagesc(stat{1}.stat)
subplot(2,2,4); imagesc(stat{2}.stat)

stat = [];
for indGroup = 1:2
    subj = size(StatStruct{indGroup}.ratespectrum,1);
    design = []; design(1,1:subj) = BetaMSE{indGroup};
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.parameter = 'ratespectrum';
    [stat{indGroup}] = ft_freqstatistics(cfgStat, StatStruct{indGroup});
end

figure; 
subplot(2,2,1); imagesc(stat{1}.stat.*stat{1}.mask)
subplot(2,2,2); imagesc(stat{2}.stat.*stat{2}.mask)
subplot(2,2,3); imagesc(stat{1}.stat)
subplot(2,2,4); imagesc(stat{2}.stat)

%% correlate with transients

addpath('/Volumes/EEG/BOSC_Sternberg/T_tools/fieldtrip-20180227'); ft_defaults;

load('/Volumes/EEG/BOSC_Sternberg/A_data/elec.mat')

ageIdx{1} = 1:47;
ageIdx{2} = 48:99;

freq = 2.^[1:.125:6];
freq = freq(4:end-3);

indCond = 2;
for indAge = 1:2
    StatStruct{indAge}.ratespectrum = squeeze(EventsNonRhythm_Merged(ageIdx{indAge},:,:,indCond));
    StatStruct{indAge}.dimord = 'rpt_chan_freq';
    StatStruct{indAge}.label = elec.label(1:60);
    StatStruct{indAge}.freq = freq;
end

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label(1:60);

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 3;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1});

BetaMSE{1} = BetaMSE_YA;
BetaMSE{2} = BetaMSE_OA;
AlphaMSE{1} = AlphaMSE_YA;
AlphaMSE{2} = AlphaMSE_OA;

stat = [];
for indGroup = 1:2
    subj = size(StatStruct{indGroup}.ratespectrum,1);
    design = []; design(1,1:subj) = AlphaMSE{indGroup};
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.parameter = 'ratespectrum';
    [stat{indGroup}] = ft_freqstatistics(cfgStat, StatStruct{indGroup});
end

figure; 
subplot(2,2,1); imagesc(stat{1}.stat.*stat{1}.mask)
subplot(2,2,2); imagesc(stat{2}.stat.*stat{2}.mask)
subplot(2,2,3); imagesc(stat{1}.stat)
subplot(2,2,4); imagesc(stat{2}.stat)

stat = [];
for indGroup = 1:2
    subj = size(StatStruct{indGroup}.ratespectrum,1);
    design = []; design(1,1:subj) = BetaMSE{indGroup};
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.parameter = 'ratespectrum';
    [stat{indGroup}] = ft_freqstatistics(cfgStat, StatStruct{indGroup});
end

figure; 
subplot(2,2,1); imagesc(stat{1}.stat.*stat{1}.mask)
subplot(2,2,2); imagesc(stat{2}.stat.*stat{2}.mask)
subplot(2,2,3); imagesc(stat{1}.stat)
subplot(2,2,4); imagesc(stat{2}.stat)

%% check age inversion of rate

addpath('/Volumes/EEG/BOSC_Sternberg/T_tools/fieldtrip-20180227'); ft_defaults;

load('/Volumes/EEG/BOSC_Sternberg/A_data/elec.mat')

ageIdx{1} = 1:47;
ageIdx{2} = 48:99;

freq = 2.^[1:.125:6];
freq = freq(4:end-3);

indCond = 2;
for indAge = 1:2
    StatStruct{indAge}.ratespectrum = squeeze(EventsRhythm_Merged(ageIdx{indAge},:,:,indCond));
    StatStruct{indAge}.dimord = 'rpt_chan_freq';
    StatStruct{indAge}.label = elec.label(1:60);
    StatStruct{indAge}.freq = freq;
end

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label(1:60);

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 3;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1});

N_1 = size(StatStruct{1}.ratespectrum,1);
N_2 = size(StatStruct{2}.ratespectrum,1);
design = zeros(2,N_1+N_2);
design(1,1:N_1) = 1;
design(1,N_1+1:end) = 2;
cfgStat.design   = design;
cfgStat.ivar     = 1;

stat = [];
cfgStat.parameter = 'ratespectrum';
[stat] = ft_freqstatistics(cfgStat, StatStruct{2}, StatStruct{1});

save([pn.data, 'AgeDifferenceRhythmRate.mat'], 'stat')

figure; 
subplot(2,1,1); imagesc(stat.stat.*stat.mask)
subplot(2,1,2); imagesc(stat.stat)

%% topography

% add colorbrewer

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
%cfg.colorbar = 'SouthOutside';
cfg.zlim = [-5 5];
cfg.style = 'both';
cfg.colormap = cBrew;

h = figure('units','normalized','position',[.1 .1 .4 .4]);
subplot(1,2,1)
    cfg.highlight = 'yes';
    cfg.highlightchannel = elec.label(max(stat.mask(:,15:19),[],2)); % 8 to 12 Hz
    plotData = [];
    plotData.label = elec.label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(median(stat.stat(:,15:19),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
    title('Alpha event rate: OA vs. YA')
subplot(1,2,2)
    cfg.highlight = 'yes';
    cfg.highlightchannel = elec.label(max(stat.mask(:,21:24),[],2)); % 14 to 20 Hz
    plotData = [];
    plotData.label = elec.label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(median(stat.stat(:,21:24),2));
    topo2 = ft_topoplotER(cfg,plotData);
    cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
    title('Beta event rate: OA vs. YA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/C_figures/';
figureName = 'C2_AgeContrastRhythmEvents';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');