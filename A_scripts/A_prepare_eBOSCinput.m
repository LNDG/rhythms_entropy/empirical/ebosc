% Prepare data for eBOSC analysis

    clear all; clc; restoredefaultpath;

%% set paths

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
    pn.dataOut  = [pn.root, 'B_analyses/D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
    pn.tools    = [pn.root, 'B_analyses/C_eBOSC_CSD/T_tools/'];
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904/'); ft_defaults

%% load subject list

    % N = 47 YA, 52 OAs
    % 2201 - no rest available; 1213 dropped (weird channel arrangement)

    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

%% get info about current subject

    for indID = 1:numel(IDs)

        idx.cond = {'EC'; 'EO'};
        numConds = length(idx.cond);

        dataConcat = [];
        for c = 1:length(idx.cond)

        %% load data

            load([pn.dataIn,  IDs{indID}, '_rest_EEG_Rlm_Fhl_rdSeg_Art_',idx.cond{c},'.mat'], 'data')

            % concatenate trials: check whether this is reasonable, as it introduces
            % hard cuts

            if c == 1
                dataConcat = data;
                dataConcat.trial = [];
                dataConcat.time = [];
                rmfield(dataConcat, 'sampleinfo');
            end

            dataConcat.trial{c} = cat(2,data.trial{:});
            dataConcat.time{c} = cat(2,data.time{:});

        end

        data = [];
        data = dataConcat; clear dataConcat;

	%% remove extraneous channels

        rem             = [];
        rem.channel     = {'all','-IOR','-LHEOG','-RHEOG','-A1','-A2'};
        rem.demean      = 'no';

        data = ft_preprocessing(rem, data); clear rem;

	%% apply CSD transformation

        % CSD transform
        csd_cfg = [];
        csd_cfg.elecfile = 'standard_1005.elc';
        csd_cfg.method = 'spline';
        data = ft_scalpcurrentdensity(csd_cfg, data);

	%% reshape for eBOSC

        for c = 1:length(idx.cond)
            dataConcat.(idx.cond{c}) = data;
            dataConcat.(idx.cond{c}).trial = dataConcat.(idx.cond{c}).trial(c);
            dataConcat.(idx.cond{c}).time = dataConcat.(idx.cond{c}).time(c);
        end

        data = [];
        data = dataConcat;

	%% save prepared output for eBOSC

        save([pn.dataOut, IDs{indID}, '_eBOSCinput.mat'], 'data');

    end