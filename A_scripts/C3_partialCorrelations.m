% perform partial correlations

figure


pn.data = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';
load([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

freq = 2.^[1:.125:6];
freq = freq(4:end-3);
 
% load stats cluster of bandpass difference

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

% correlate with individual MSE values

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat')

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

%corr(AlphaRate_YA, AlphaMSE_YA)
[r,p] = partialcorr(AlphaRate_YA, AlphaMSE_YA, [BetaRate_YA, BetaMSE_YA])
%corr(AlphaRate_OA, AlphaMSE_OA)
[r,p] = partialcorr(AlphaRate_OA, AlphaMSE_OA, [BetaRate_OA, BetaMSE_OA])

%corr(BetaRate_YA, BetaMSE_YA)
[r,p] = partialcorr(BetaRate_YA, BetaMSE_YA, [AlphaRate_YA, AlphaMSE_YA])
%corr(BetaRate_OA, BetaMSE_OA)
[r,p] = partialcorr(BetaRate_OA, BetaMSE_OA, [AlphaRate_OA, AlphaMSE_OA])

figure; hold on; 
[~,~,BetaRate_YA_r] = regress(BetaRate_YA,[ones(size(AlphaRate_YA)),AlphaRate_YA, AlphaMSE_YA]);
[~,~,BetaMSE_YA_r] = regress(BetaMSE_YA,[ones(size(AlphaRate_YA)),AlphaMSE_YA, AlphaRate_YA]);
scatter(BetaMSE_YA_r, BetaRate_YA_r, 'filled')
[r,p] = corrcoef(BetaMSE_YA_r, BetaRate_YA_r)
[~,~,BetaRate_OA_r] = regress(BetaRate_OA,[ones(size(AlphaRate_OA)),AlphaRate_OA,AlphaMSE_OA]);
[~,~,BetaMSE_OA_r] = regress(BetaMSE_OA,[ones(size(AlphaMSE_OA)),AlphaMSE_OA,AlphaRate_OA]);
scatter(BetaMSE_OA_r, BetaRate_OA_r, 'filled')
[r,p] = corrcoef(BetaMSE_OA_r, BetaRate_OA_r)

figure; hold on; 
[~,~,AlphaRate_YA_r] = regress(AlphaRate_YA,[ones(size(AlphaRate_YA)),BetaRate_YA,BetaMSE_YA]);
[~,~,AlphaMSE_YA_r] = regress(AlphaMSE_YA,[ones(size(AlphaRate_YA)),BetaMSE_YA,BetaRate_YA]);
scatter(AlphaRate_YA_r, AlphaMSE_YA_r, 'filled')
[r,p] = corrcoef(AlphaRate_YA_r, AlphaMSE_YA_r)
[~,~,AlphaRate_OA_r] = regress(AlphaRate_OA,[ones(size(BetaRate_OA)),BetaRate_OA,BetaMSE_OA]);
[~,~,AlphaMSE_OA_r] = regress(AlphaMSE_OA,[ones(size(BetaMSE_OA)),BetaMSE_OA,BetaRate_OA]);
scatter(AlphaRate_OA_r, AlphaMSE_OA_r, 'filled')
[r,p] = corrcoef(AlphaRate_OA_r, AlphaMSE_OA_r)

figure
scatter(AlphaRate_YA_r, BetaRate_YA_r)
scatter(AlphaRate_OA_r, BetaRate_OA_r)

[r,p] = corrcoef(AlphaRate_YA_r, BetaRate_YA_r)
[r,p] = corrcoef(AlphaRate_OA_r, BetaRate_OA_r)

% control only for the rate of the other frequency

% figure; hold on; 
% [~,~,BetaRate_YA_r] = regress(BetaRate_YA,[ones(size(AlphaRate_YA)),AlphaRate_YA]);
% [~,~,BetaMSE_YA_r] = regress(BetaMSE_YA,[ones(size(AlphaRate_YA)),AlphaMSE_YA]);
% scatter(BetaMSE_YA_r, BetaRate_YA_r)
% [r,p] = corrcoef(BetaMSE_YA_r, BetaRate_YA_r)
% [~,~,BetaRate_OA_r] = regress(BetaRate_OA,[ones(size(AlphaRate_OA)),AlphaRate_OA]);
% [~,~,BetaMSE_OA_r] = regress(BetaMSE_OA,[ones(size(AlphaMSE_OA)),AlphaMSE_OA]);
% scatter(BetaMSE_OA_r, BetaRate_OA_r)
% [r,p] = corrcoef(BetaMSE_OA_r, BetaRate_OA_r)
% 
% figure; hold on; 
% [~,~,AlphaRate_YA_r] = regress(AlphaRate_YA,[ones(size(AlphaRate_YA)),BetaRate_YA]);
% [~,~,AlphaMSE_YA_r] = regress(AlphaMSE_YA,[ones(size(AlphaRate_YA)),BetaMSE_YA]);
% scatter(AlphaMSE_YA_r, AlphaRate_YA_r)
% [r,p] = corrcoef(AlphaRate_YA_r, AlphaMSE_YA_r)
% [~,~,AlphaRate_OA_r] = regress(AlphaRate_OA,[ones(size(BetaRate_OA)),BetaRate_OA]);
% [~,~,AlphaMSE_OA_r] = regress(AlphaMSE_OA,[ones(size(BetaMSE_OA)),BetaMSE_OA]);
% scatter(AlphaMSE_OA_r, AlphaRate_OA_r)
% [r,p] = corrcoef(AlphaRate_OA_r, AlphaMSE_OA_r)
% 
% figure
% scatter(AlphaRate_YA_r, BetaRate_YA_r)
% scatter(AlphaRate_OA_r, BetaRate_OA_r)

figure
subplot(1,2,1); cla; hold on;
    x = AlphaRate_YA_r; y = AlphaMSE_YA_r; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA_r; y = AlphaMSE_OA_r; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events (residual)'); ylabel('Entropy (residual)');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaRate_YA_r; y = BetaMSE_YA_r; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA_r; y = BetaMSE_OA_r; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events (residual)'); ylabel('Entropy (residual)');
    %ylim([.3 .5])
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

