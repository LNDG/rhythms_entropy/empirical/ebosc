%% load subject list

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';

load([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

%% plot

freq = 2.^[1:.125:6];
freq = freq(4:end-3);

figure;
subplot(3,2,1); imagesc(squeeze(nanmean(Events_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,2); imagesc(squeeze(nanmean(Events_Merged(:,:,:,2),1))./...
    squeeze(nanmean(Events_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,3); imagesc(squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,4); imagesc(squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,2),1))./...
    squeeze(nanmean(EventsNonRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,5); imagesc(squeeze(nanmean(EventsRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
subplot(3,2,6); imagesc(squeeze(nanmean(EventsRhythm_Merged(:,:,:,2),1))./...
    squeeze(nanmean(EventsRhythm_Merged(:,:,:,1),1)))
    set(gca, 'XTickLabels', round(freq(get(gca, 'XTick')),0));
    
%% topography of events in alpha band

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/elec.mat')

% add colorbrewer

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904/'); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-5 5];
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.marker = 'off';  
cfg.highlight = 'yes';
cfg.highlightchannel = elec.label([44:50, 52:56, 58:60]);
cfg.highlightcolor = [0 0 0];
cfg.highlightsymbol = '.';
cfg.highlightsize = 40;

h = figure('units','normalized','position',[.1 .1 .1 .2]);
    plotData = [];
    plotData.label = elec.label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(EventsRhythm_Merged(:,:,freq>=8 & freq<=15,2),3),1))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','# detected events');
    title('Alpha event rate')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/C_figures/';
figureName = 'F_rhythmRateAlpha';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');