function B_eBOSC_CSD_noDur(ID)

% Conduct BOSC + eBOSC analysis

%clear all; clc; restoredefaultpath;

%% set paths

if ismac
    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'B_analyses/D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
    pn.dataOut  = [pn.root, 'B_analyses/D_eBOSC_noDur/B_data/B_eBOSCout/'];
    pn.tools    = [pn.root, 'B_analyses/D_eBOSC_noDur/T_tools/'];
    addpath(genpath([pn.tools, 'eBOSC-master/'])); 
else
    pn.root = '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/';
    pn.dataIn   = [pn.root, 'B_data/A_dataPreBOSC/'];
    pn.dataOut  = [pn.root, 'B_data/B_eBOSCout/'];
end

%%  BOSC parameters

cfg.eBOSC.F                 = 2.^[1:.125:6];                            % frequency sampling (~Whitten et al., 2011), but higher frequency resolution
cfg.eBOSC.wavenumber        = 5;                                        % wavelet family parameter (time-frequency tradeoff) [recommended: ~6]
cfg.eBOSC.ncyc              = repmat(1, 1, numel(cfg.eBOSC.F));         % vector of duration thresholds at each frequency
cfg.eBOSC.percentile        = .95;                                      % percentile of background fit for power threshold
cfg.eBOSC.fsample           = 500;                                      % current sampling frequency of EEG data
cfg.eBOSC.WLpadding         = 500;                                      % padding to avoid edge artifacts due to WL [SPs]
cfg.eBOSC.detectedPad       = 250;                                      % 'shoulder' for BOSC detected matrix to account for duration threshold
cfg.eBOSC.trialPad          = cfg.eBOSC.WLpadding+cfg.eBOSC.detectedPad;    % complete padding (WL + shoulder)
cfg.eBOSC.BGpad             = cfg.eBOSC.trialPad;                           % use same effective signal as for subsequent sinbgle-trial analyses
cfg.eBOSC.fres              = 1.2;                                          % cf. Linkenkaer-Hansen, K., et al. (2001). "Long-Range Temporal Correlations and Scaling Behavior in Human Brain Oscillations." The Journal of Neuroscience 21(4): 1370-1377.
cfg.eBOSC.fstp              = 1;
cfg.eBOSC.freqRemoval       = 'JQK';
cfg.eBOSC.BiasCorrection    = 'yes';
cfg.eBOSC.method            = 'MaxBias';
cfg.eBOSC.edgeOnly          = 'no';
cfg.eBOSC.effSignal         = 'PT';
cfg.eBOSC.LowFreqExcludeBG  = 8;
cfg.eBOSC.HighFreqExcludeBG = 15;

%% load subject list

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

%% get info about current subject

    
    idx.cond = {'EC'; 'EO'};
    numConds = length(idx.cond);
    numFreqs = numel(cfg.eBOSC.F);
                
    %% load data

    load([pn.dataIn,  ID, '_eBOSCinput.mat'], 'data')

    %%%%%%%%%%%
    %% eBOSC %%
    %%%%%%%%%%%
    
    %% oscillation detection 
    
    for e = 1:60 % electrode loop

        % display progress
        display([ID ', channel #' num2str(e)])
        tic
        
        for c = 1:length(idx.cond)
        
            %% get eBOSC timing
            
            bosc.inputTime = data.(idx.cond{c}).time{1,1};
            bosc.detectedTime = bosc.inputTime(cfg.eBOSC.WLpadding+1:end-cfg.eBOSC.WLpadding);
            bosc.finalTime = bosc.inputTime(cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad);
            
            %% wavelet decomposition
    
            numTrials = length(data.(idx.cond{c}).trial);

            TFRData = [];
            for t = 1:numTrials % trial index
                TFRData.(idx.cond{c}).trial{t} = BOSC_tf(data.(idx.cond{c}).trial{t}(e,:),...
                    cfg.eBOSC.F,cfg.eBOSC.fsample,cfg.eBOSC.wavenumber);
            end; clear t
                        
            %% condition-specific background calculation
            % Concatenate single-trials in the periods that final detection
            % will be based on for background calculation.
            
            BG = [];
            for t = 1:numTrials
                BG = [BG TFRData.(idx.cond{c}).trial{t}(:,cfg.eBOSC.BGpad+1:end-cfg.eBOSC.BGpad)];
            end; clear t
            
            %% standard backgrounds
            
            % background power estimation - standard
            [pv_stnd,~] = BOSC_bgfit(cfg.eBOSC.F,BG); 
            mp_stnd = 10.^(polyval(pv_stnd,log10(cfg.eBOSC.F))); 

            % background power estimation - robust
            % find peak between 8-15 Hz, get wavelet extension in frequency domain, remove
            % points within this range from the estimation; compute robust
            % regression
            
            freqInd1 = find(cfg.eBOSC.F >= cfg.eBOSC.LowFreqExcludeBG, 1, 'first');
            freqInd2 = find(cfg.eBOSC.F <= cfg.eBOSC.HighFreqExcludeBG, 1, 'last');
            
            [~, indPos] = max(mean(BG(freqInd1:freqInd2,:),2));
            indPos = freqInd1+indPos;
            
            LowFreq = cfg.eBOSC.F(indPos)-(((2/cfg.eBOSC.wavenumber)*cfg.eBOSC.F(indPos))/2);
            UpFreq = cfg.eBOSC.F(indPos)+(((2/cfg.eBOSC.wavenumber)*cfg.eBOSC.F(indPos))/2);
            
            freqIndLow = find(cfg.eBOSC.F >= LowFreq, 1, 'first');
            freqIndHigh = find(cfg.eBOSC.F <= UpFreq, 1, 'last');
            
            [pv,~] = eBOSC_bgfit_robust(cfg.eBOSC.F([1:freqIndLow-1 freqIndHigh+1:end]),BG([1:freqIndLow-1 freqIndHigh+1:end], :));
            mp = 10.^(polyval(pv,log10(cfg.eBOSC.F))); 

            % BOSC thresholds
            [pt,dt] = BOSC_thresholds(cfg.eBOSC.fsample,cfg.eBOSC.percentile,cfg.eBOSC.ncyc,cfg.eBOSC.F,mp);

            % keep overall background, 1/f fit, and power threshold
            BGinfo.(idx.cond{c}).bg_pow(e,:)        = mean(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad),2);
            BGinfo.(idx.cond{c}).bg_log10_pow(e,:)  = mean(log10(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
            BGinfo.(idx.cond{c}).bg_amp(e,:)        = mean(sqrt(BG(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
            BGinfo.(idx.cond{c}).pv(e,:)            = pv;
            BGinfo.(idx.cond{c}).mp(e,:)            = mp;
            BGinfo.(idx.cond{c}).mp_stnd(e,:)       = mp_stnd;
            BGinfo.(idx.cond{c}).pt(e,:)            = pt;

            % clear variables
            clear pv pv_stnd mp_stnd
            
            for t = 1:numTrials

                 % initialize variables
                bosc.(idx.cond{c}).pepisode{1,t}(e,:)  = zeros(1,numFreqs);
                bosc.(idx.cond{c}).abundance{1,t}(e,:) = zeros(1,numFreqs);
                bosc.(idx.cond{c}).episodes{1,t}{e,1}  = [];

                % wavelet transform
                TrialTFR = TFRData.(idx.cond{c}).trial{1,t}; 

            %%  initial BOSC rhythm detection
            
                % WLpadding is removed to avoid edge artifacts during the
                % detection. Note that detectedPad still remains so that there
                % is no problems with too few sample points at the edges to
                % fulfill the numcycles criterion.

                effectiveTrialTFR = TrialTFR(:,cfg.eBOSC.WLpadding+1:end-cfg.eBOSC.WLpadding);
                
                detected = zeros(size(effectiveTrialTFR));
                for f = 1:numFreqs
                    detected(f,:) = BOSC_detect(effectiveTrialTFR(f,:),pt(f),dt(f),cfg.eBOSC.fsample);
                end; clear f
                
                % Pepisode = length of detected rhythm SPs relative to
                % segment length. Here, we discard detectedPad sample points 
                % at each end to account for the numcycles requirement.
                bosc.(idx.cond{c}).pepisode{1,t}(e,:) = mean(detected(:,cfg.eBOSC.detectedPad+1:end-cfg.eBOSC.detectedPad),2);
                
                %%  abundance estimation (THG's BOSC extension, adapted by JQK)

                cfg.eBOSC.npnts = size(detected,2);
                cfg.eBOSC.pt  = pt;
            
                episodes = [];
                [detected_abn,episodes] = eBOSC_createEpisodes(effectiveTrialTFR,detected,cfg);

                bosc.(idx.cond{c}).abundance{1,t}(e,:) = mean(detected_abn(:,cfg.eBOSC.detectedPad+1:end-cfg.eBOSC.detectedPad),2); 

                cfg.eBOSC = rmfield(cfg.eBOSC,'npnts');
                cfg.eBOSC = rmfield(cfg.eBOSC,'pt');
                
                % extra: plot detected vs detected1 power
                % figure; subplot(121); imagesc(detected.*effectiveTrialTFR); 
                % subplot(122); imagesc(detected_abn.*effectiveTrialTFR);
                % figure; hold on; plot(squeeze(nanmean(detected,2))); plot(squeeze(nanmean(detected_abn,2)))
                
                % Save overall spectral power of the segment

                bosc.(idx.cond{c}).spctrm.o_pow{1,t}(e,:)       = mean(TrialTFR(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad),2); % rm 1 sec at beginning and end
                bosc.(idx.cond{c}).spctrm.o_log10_pow{1,t}(e,:) = mean(log10(TrialTFR(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
                bosc.(idx.cond{c}).spctrm.o_amp{1,t}(e,:)       = mean(sqrt(TrialTFR(:,cfg.eBOSC.trialPad+1:end-cfg.eBOSC.trialPad)),2);
                
                %% remove padded segments from episodes
                % The extended episode detection has as an input the padded
                % detected matrix. Therefore, the episodes that have been
                % defined based on the new extended detected matrix have to be
                % cut appropriately to discard the padding.

                ind1 = cfg.eBOSC.detectedPad+1;
                ind2 = size(detected_abn,2) - cfg.eBOSC.detectedPad;
                cnt = 1;
                rmv = [];
                for indEp = 1:size(episodes,1)
                    % final episodes
                    ex = find(episodes{indEp,1}(:,2) < ind1 | episodes{indEp,1}(:,2) > ind2);
                    % update episodes
                    episodes{indEp,1}(ex,:) = [];
                    episodes{indEp,1}(:,2) = episodes{indEp,1}(:,2) - ind1 + 1; 
                    episodes{indEp,2}(ex,:) = [];
                    episodes{indEp,3}       = mean(episodes{indEp,2}(:,1));
                    episodes{indEp,4}       = size(episodes{indEp,1},1) / cfg.eBOSC.fsample; clear ex
                    if isempty(episodes{indEp,1})
                        rmv(cnt,1) = indEp;
                        cnt = cnt + 1;
                    end
                end; clear indEp cnt
                episodes(rmv,:) = []; clear rmv
                
                episodes = episodes(:,1:4);
                
        %% save data in structure
        
            % episodes
            bosc_episodes{c,t}  = episodes;

            %%  clear variables

            clear abundance detected episodes ind* pepisode spctrm TrialTFR

        end; clear t;
        end; clear c;
        
        save([pn.dataOut ID, '_eBOSC_Chan',num2str(e),'.mat'],'bosc_episodes')
        
        clear bosc_episodes;
    
    end; clear e; % electrode loop

    save([pn.dataOut ID, '_MatrixDetected_v1.mat'],'results', 'cfg')

end % function end