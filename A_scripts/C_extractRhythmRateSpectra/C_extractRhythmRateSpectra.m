% Collect rhythm characteristics of detected rhythms with mean frequency
% between lowFreq and highFreq

function C_extractRhythmRateSpectra(ID)

% boundaries are center frequencies +- 3 adjacent frequency bands Hz

    freq = 2.^[1:.125:6];

    for indBound = 4:numel(freq)-3
        RhythmBoundaries(1, indBound-3) = freq(indBound-3);
        RhythmBoundaries(2, indBound-3) = freq(indBound+3);
    end

    if ismac
        pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/';
    else
        pn.root = '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/';
    end
    pn.boscOut  = [pn.root, 'B_data/B_eBOSCout/'];
    pn.dataOut  = [pn.root, 'B_data/C_RhythmCharacteristics/'];
        
    for e = 1:60
   
        load([pn.boscOut ID, '_eBOSC_Chan',num2str(e),'.mat'],'bosc_episodes'); % load data

        for indCond = 1:2
            allEps = cat(1,bosc_episodes{indCond,:});
            for indBound = 1:size(RhythmBoundaries,2)
                lowFreq = RhythmBoundaries(1, indBound);
                highFreq = RhythmBoundaries(2, indBound);
            
                % Column 4 encodes the duration of rhythmicity in seconds!
                % cycle duration is calculated by multiplying with mean frequency

                CycleDuration = cell2mat(allEps(:,3)).*cell2mat(allEps(:,4));
                criteria = cell2mat(allEps(:,3)) >= lowFreq & ...
                    cell2mat(allEps(:,3)) <= highFreq;

                Events(e,indBound,indCond) = numel(find(criteria==1))./numel(find(~cellfun(@isempty,bosc_episodes(indCond,:))));
                
                criteria = cell2mat(allEps(:,3)) >= lowFreq & ...
                cell2mat(allEps(:,3)) <= highFreq & ...
                CycleDuration >= 3;
                EventsRhythm(e,indBound,indCond) = numel(find(criteria==1))./numel(find(~cellfun(@isempty,bosc_episodes(indCond,:))));
               
                criteria = cell2mat(allEps(:,3)) >= lowFreq & ...
                cell2mat(allEps(:,3)) <= highFreq & ...
                CycleDuration < 3;
                EventsNonRhythm(e,indBound,indCond) = numel(find(criteria==1))./numel(find(~cellfun(@isempty,bosc_episodes(indCond,:))));
                
            end
        end
    end
    
    save([pn.dataOut, 'RhythmRate_', ID, '.mat'], 'Events*');
