#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% go to analysis directory containing .m-file
cd('/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/A_scripts/C_extractRhythmRateSpectra/')
%% compile function and append dependencies
mcc -m C_extractRhythmRateSpectra.m
exit