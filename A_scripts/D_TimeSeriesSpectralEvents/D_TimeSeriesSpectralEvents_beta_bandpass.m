%C_PlotTimeSeriesRhythms

    if ismac
        %pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/';
        pn.root = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/';
        pn.dataIn   = [pn.root, 'D_eBOSC_noDur/B_data/A_dataPreBOSC/'];
        pn.boscOut  = [pn.root, 'D_eBOSC_noDur/B_data/B_eBOSCout/'];
        pn.dataOut = [pn.root, 'D_eBOSC_noDur/B_data/C_RhythmCharacteristics/'];
        pn.tools    = [pn.root, 'D_eBOSC_noDur/T_tools/'];
        addpath(genpath([pn.tools, 'eBOSC-master/'])); 
        addpath('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/E_permMSE/T_tools/fieldtrip-20170904/'); ft_defaults;
    else
        pn.root = '/home/mpib/kosciessa/xxx/';
    end

    %% settings
    
    lowFreq = '14'; highFreq = '20';
    
    %% eBOSC params
    
    cfg.eBOSC.WLpadding         = 500;                                         % 4 seconds shoulder removal for trial data (note: no removal for detected matrix; still 1 s peri-retention 'padding')
    cfg.eBOSC.detectedPad       = 250;                                          % 'shoulder' for BOSC detected matrix to account for duration threshold
    cfg.eBOSC.trialPad          = cfg.eBOSC.WLpadding+cfg.eBOSC.detectedPad;    % complete padding (WL + shoulder)
    cfg.eBOSC.BGpad             = cfg.eBOSC.trialPad;  
    
    %% IDs 
    
   % N = 47 YA, 52 OAs
    % 2201 - no rest available; 1213 dropped (weird channel arrangement)

    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};
        
        TimeEps = [];
        idx.cond = {'EC'; 'EO'};
        
        for indID = 1:32
        
            ID = IDs{indID};
            
            %% load time domain data

            load([pn.dataIn,  ID, '_eBOSCinput.mat'], 'data')

            %% low-pass filter data
            
            dataOrig = data;
            
            for c = 1:2
                bp.bpfilter    = 'yes';
                bp.bpfreq      = [10, 25];
                bp.bpfiltord   = 6;
                
                data.(idx.cond{c}) = ft_preprocessing(bp, data.(idx.cond{c}));
            end

            %% extract time from rhythmic episodes

            for e = find(ismember(data.EC.label, 'Cz'))

                load([pn.boscOut ID, '_eBOSC_Chan',num2str(e),'.mat'],'bosc_episodes'); % load data

                TimeEps{indID, indCond} = cell(1);
                
                for indCond = 1:2
                    epCount = 1;
                    for indTrial = 1:numel(bosc_episodes(indCond,:))
                        disp(['Condition ', num2str(indCond), ' Trial ', num2str(indTrial)]);
                        if isempty(bosc_episodes{indCond,indTrial})
                            continue;
                        end
                        allEps = bosc_episodes{indCond,indTrial};

                        % Column 4 encodes the duration of rhythmicity in seconds!
                        % cycle duration is calculated by multiplying with mean frequency
                        CycleDuration = cell2mat(allEps(:,3)).*cell2mat(allEps(:,4));

                        criteria = cell2mat(allEps(:,3)) >= str2num(lowFreq) & ...
                            cell2mat(allEps(:,3)) <= str2num(highFreq) & ...
                            CycleDuration >= 1;

                        EpsInRange = allEps(criteria,:);

                        % pad with 2 s at each end, i.e. 500 sample points
                        if ~isempty(EpsInRange)
                            for indEp = 1:size(EpsInRange,1)
                                relevantTimePoints = cfg.eBOSC.BGpad+EpsInRange{indEp,1}(1,2)-500:cfg.eBOSC.BGpad+EpsInRange{indEp,1}(1,2)+500;
                                episodeTime = data.(idx.cond{indCond}).trial{indTrial}(e,relevantTimePoints);
                                episodeTimeOrig = dataOrig.(idx.cond{indCond}).trial{indTrial}(e,relevantTimePoints);
                                %figure; plot(zscore(EpsInRange{indEp,2}(:,2))); hold on; plot(zscore(episodeTime), 'r')
                                TimeEps{indID, indCond}{epCount,1} = episodeTime;
                                %TimeEps{indID, indCond}{epCount,2} = zscore(episodeTime);
                                TimeEps{indID, indCond}{epCount,2} = episodeTimeOrig;
                                TimeEps{indID, indCond}{epCount,3} = zscore(episodeTimeOrig);
                                TimeEps{indID, indCond}{epCount,4} = [zeros(500,1); EpsInRange{indEp,2}(:,2);zeros(500,1)];
                                TimeEps{indID, indCond}{epCount,5} = nanmean(EpsInRange{indEp,2}(:,2));
                                TimeEps{indID, indCond}{epCount,6} = EpsInRange{indEp,3};
                                epCount = epCount+1;
                            end
                        end

                        clear criteria EpsInRange allEps CycleDuration;
                        
                    end % trial
                end % condition
            end % channel
        end % ID
        
        %% save data
        
        save([pn.dataOut, 'D_TimeDomainRhythmicity_Beta_lowpass.mat'],'TimeEps');