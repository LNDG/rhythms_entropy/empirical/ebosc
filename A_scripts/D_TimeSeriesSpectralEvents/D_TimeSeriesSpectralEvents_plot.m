%% plot bandpassed versions in overview figure: alpha, beta

  h = figure('units','normalized','position',[.1 .1 .5 .2]);
        subplot(1,2,1); cla; hold on;
        load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Alpha_lowpass.mat')
            for indID = 1:32
               curData = cat(1, TimeEps{indID,:});
               ampData = curData(:,4);
               curDataPlot = curData(:,3);
               curData = curData(:,1);
               tempSeries = NaN(size(curData,1),3000);
               for indEp = 1:size(curData,1)
                     % find minima clostest to TFR maximum 
                    [~, sortIndTFR] = max(ampData{indEp});
                    TF = islocalmin(curData{indEp});
                    TF = find(TF);
                    [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                    sortInd = TF(minInd_tmp);
                    tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                    tempSeriesDur(indEp) = numel(curData{indEp});
                    tempSeriesDurPre(indEp) = 500+sortInd;
                    tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
               end
               AvgSeries_alpha(indID,:) = nanmedian(tempSeries,1);
               time = -1500*1/250:1/250:1500*1/250;
               plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
               xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_alpha,1), 'Color', 'k', 'LineWidth', 4)
            title('Alpha events time-locked to alpha minimum (8-12 Hz; POz)')
            ylim([-2.5 2.5])
       subplot(1,2,2); cla; hold on;
        load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Beta_lowpass.mat')
            for indID = 1:32
                try
                   curData = cat(1, TimeEps{indID,2});
                   ampData = curData(:,4);
                   curDataPlot = curData(:,3);
                   curData = curData(:,1);
                   tempSeries = NaN(size(curData,1),3000);
                   for indEp = 1:size(curData,1)
                         % find minima clostest to TFR maximum 
                        [~, sortIndTFR] = max(ampData{indEp});
                        TF = islocalmin(curData{indEp});
                        TF = find(TF);
                        [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                        sortInd = TF(minInd_tmp);
                        tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                        tempSeriesDur(indEp) = numel(curData{indEp});
                        tempSeriesDurPre(indEp) = 500+sortInd;
                        tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
                   end
                   time = -1500*1/250:1/250:1500*1/250;
                   plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
                   AvgSeries_beta(indID,:) = nanmedian(tempSeries,1);
                   xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
                catch
                    continue;
                end
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_beta,1), 'Color', 'k', 'LineWidth', 4)
            title('Beta events time-locked to beta minimum (14-20 Hz; Cz)')
            ylim([-1.75 1.75]) 
       set(findall(gcf,'-property','FontSize'),'FontSize',16)

    pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/C_figures/';
    figureName = 'D_timeSeriesRhythms';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    %% save alpha and beta separately
    
     h = figure('units','normalized','position',[.1 .1 .25 .2]);
     cla; hold on;
        load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Beta_lowpass.mat')
            for indID = 1:32
                try
                   curData = cat(1, TimeEps{indID,2});
                   ampData = curData(:,4);
                   curDataPlot = curData(:,3);
                   curData = curData(:,1);
                   tempSeries = NaN(size(curData,1),3000);
                   for indEp = 1:size(curData,1)
                         % find minima clostest to TFR maximum 
                        [~, sortIndTFR] = max(ampData{indEp});
                        TF = islocalmin(curData{indEp});
                        TF = find(TF);
                        [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                        sortInd = TF(minInd_tmp);
                        tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                        tempSeriesDur(indEp) = numel(curData{indEp});
                        tempSeriesDurPre(indEp) = 500+sortInd;
                        tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
                   end
                   time = -1500*1/250:1/250:1500*1/250;
                   plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
                   AvgSeries_beta(indID,:) = nanmedian(tempSeries,1);
                   xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
                catch
                    continue;
                end
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_beta,1), 'Color', 'k', 'LineWidth', 4)
            title('Beta events time-locked to beta minimum (14-20 Hz; Cz)')
            ylim([-1.75 1.75]) 
       set(findall(gcf,'-property','FontSize'),'FontSize',16)

    pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/C_figures/';
    figureName = 'D_timeSeriesRhythms_beta';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    h = figure('units','normalized','position',[.1 .1 .25 .2]);
     cla; hold on;
        load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Alpha_lowpass.mat')
            for indID = 1:32
               curData = cat(1, TimeEps{indID,:});
               ampData = curData(:,4);
               curDataPlot = curData(:,3);
               curData = curData(:,1);
               tempSeries = NaN(size(curData,1),3000);
               for indEp = 1:size(curData,1)
                     % find minima clostest to TFR maximum 
                    [~, sortIndTFR] = max(ampData{indEp});
                    TF = islocalmin(curData{indEp});
                    TF = find(TF);
                    [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                    sortInd = TF(minInd_tmp);
                    tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                    tempSeriesDur(indEp) = numel(curData{indEp});
                    tempSeriesDurPre(indEp) = 500+sortInd;
                    tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
               end
               AvgSeries_alpha(indID,:) = nanmedian(tempSeries,1);
               time = -1500*1/250:1/250:1500*1/250;
               plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
               xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_alpha,1), 'Color', 'k', 'LineWidth', 4)
            title('Alpha events time-locked to alpha minimum (8-12 Hz; POz)')
            ylim([-2.5 2.5])
       set(findall(gcf,'-property','FontSize'),'FontSize',16)

    pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/C_figures/';
    figureName = 'D_timeSeriesRhythms_alpha';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
